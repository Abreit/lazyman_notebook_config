import pandas as p....

NO MORE !!!! All your favorite packages automatically imported at launch and matplotlib, seaborn configured for notebook display! 

![](adieu_pieton.jpg)

# lazyman_notebook_config


# install


```python
!pip install git+https://gitlab.com/Abreit/lazyman_notebook_config.git
```


# deploy

```python
from lazyman_notebook_config.management import deploy,display_lazy_imports,disable,add_import_packages,remove
```

```python
deploy()
```

# add packages


```python
add_import_packages([['sklearn','sklearn'],['gpd','geopandas']]
                   )
display_lazy_imports()
```

    ========== IMPORTED BASIC MODULES ============
    import importlib
    from pathlib import Path
    import sys
    import os
    import json
    from pkg_resources import resource_filename
    import copy
        
    ========== IMPORTED MODULES ============
    module geopandas not imported : No module named 'geopandas'
    module matplotlib imported as mpl, version : 3.2.2
    module numpy imported as np, version : 1.18.5
    module pandas imported as pd, version : 1.0.5
    module matplotlib.pyplot imported as plt
    module sklearn imported as sklearn, version : 0.23.1
    module seaborn imported as sns, version : 0.10.1
    

# disable/remove

disable lazyman_notebook_config. import wil have to be done manually


```python
disable()
```

    lazyman notebook config has been disabled
    

remove entirely lazyman_notebook_config


```python
remove()
```


# use

restart notebook from here


```python
sklearn
```




    <module 'sklearn' from 'C:\\ProgramData\\Anaconda3\\lib\\site-packages\\sklearn\\__init__.py'>




```python
pd.DataFrame([0,1,1]).plot()
```
