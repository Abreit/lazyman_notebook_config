from pathlib import Path
from pkg_resources import resource_filename
from shutil import copy, rmtree
import json
import importlib
import numpy as np
from . import lazy_name, lagaffe_ascii_art, __version__

# IPYTHON CFG PATHS
profile_default_path = Path().home() / '.ipython/profile_default'
startup_dir = (profile_default_path / 'startup')
cfg_file_ipython = startup_dir / 'load_lazyman_config.py'
cfg_module_list_file_ipython = startup_dir.parent / 'lazyman_modules.json'


def deploy(replace_lazyman_modules_file=False):
    if not profile_default_path.is_dir():
        NotADirectoryError(
            f'{profile_default_path} is not a valid directory. cannot deploy {lazy_name}')

    if not startup_dir.is_dir():
        print(f'making ipython startup dir : {startup_dir}')
        startup_dir.mkdir()

    cfg_file = resource_filename('lazyman_notebook_config', 'notebook_config_script/load_lazyman_config.py')
    with open(cfg_file, 'r') as src:
        with open(cfg_file_ipython, 'w') as dest:
            dest.write(src.read())

    if not cfg_module_list_file_ipython.is_file() | replace_lazyman_modules_file:
        cfg_module_list_file = resource_filename('lazyman_notebook_config', 'lazyman_modules.json')
        with open(cfg_module_list_file, 'r') as src:
            with open(cfg_module_list_file_ipython, 'w') as dest:
                dest.write(src.read())
    print(f'lazyman_notebook_config {__version__} deployed. All your notebooks will be loaded with the lazyman config.')
    display_lazy_imports()
    print(lagaffe_ascii_art)


def display_lazy_imports():
    print("""
========== IMPORTED BASIC MODULES ============
import importlib
from pathlib import Path
import sys
import os
import json
from pkg_resources import resource_filename
import copy
    """)

    print('========== IMPORTED MODULES ============')

    def load_module(abv, module_name):
        try:
            module = importlib.import_module(module_name)
            globals()[abv] = module
            # submodules dont have versions.
            if '.' in module_name:
                print(f'module {module_name} imported as {abv}')
            elif hasattr(module, '__version__'):
                print(f'module {module_name} imported as {abv}, version : {module.__version__}')
            elif hasattr(module, 'version'):
                print(f'module {module_name} imported as {abv}, version : {module.__version__}')
            else:
                print(f'module {module_name} imported as {abv}')

        except ModuleNotFoundError as e:
            print(f'module {module_name} not imported : {e}')

    with open(cfg_module_list_file_ipython, 'r') as f:
        modules = json.load(f)

        for module in modules:
            load_module(*module)


def disable():
    print('lazyman notebook config has been disabled')
    with open(cfg_file_ipython, 'w') as f:
        f.write('')


def remove():
    Path(cfg_file_ipython).unlink()
    Path(cfg_module_list_file_ipython).unlink()


def add_import_packages(pkg_list):
    with open(cfg_module_list_file_ipython, 'r') as f:
        modules = json.load(f)
    modules.extend(pkg_list)
    modules = np.unique(modules,axis=0).tolist()
    print(modules)
    with open(cfg_module_list_file_ipython, 'w') as f:
        json.dump(modules, f, indent=4)
