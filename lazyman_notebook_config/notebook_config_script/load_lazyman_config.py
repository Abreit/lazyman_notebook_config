import importlib
from pathlib import Path
import sys
import os
import json
from pkg_resources import resource_filename
import copy

print("""
========== IMPORT BASICS ============
import importlib
from pathlib import Path
import sys
import os
import json
from pkg_resources import resource_filename
import copy
""")


def load_module(abv, module_name):
    try:
        module = importlib.import_module(module_name)
        globals()[abv] = module
        # submodules dont have versions.
        if '.' in module_name:
            print(f'module {module_name} imported as {abv}')
        elif hasattr(module, '__version__'):
            print(f'module {module_name} imported as {abv}, version : {module.__version__}')
        elif hasattr(module, 'version'):
            print(f'module {module_name} imported as {abv}, version : {module.__version__}')
        else:
            print(f'module {module_name} imported as {abv}')

    except ModuleNotFoundError as e:
        print(f'module {module_name} not imported : {e}')


modules_path = Path(__file__).absolute().parent.parent / 'lazyman_modules.json'
with open(modules_path, 'r') as f:
    modules = json.load(f)

print('========== IMPORT MODULES ============')
for module in modules:
    load_module(*module)



try:
    get_ipython().magic('matplotlib inline')
except:
    print('cant set matplotlib inline')

if "sns" in globals():
    sns.set_context('talk')

if 'mpl' in globals():
    mpl.rcParams['figure.figsize'] = (17, 8)

class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

print('lazyman_notebook_config loaded.')