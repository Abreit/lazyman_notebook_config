from setuptools import find_packages, setup


version = '1.0.0'

setup(
    name='lazyman_notebook_config',
    version=version,
    url='https://gitlab.com/Abreit/lazyman_notebook_config',
    packages=find_packages(),
    install_requires=['numpy'],
    package_data={'':['*.txt','*.json','*.py'],
    'notebook_config_script':['*.py']})


